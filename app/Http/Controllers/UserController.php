<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        return view('users/list');
    }

    public function list()
    {
        $users = User::get()->toArray();
        $result['data'] = $users;
        $result['edit_url'] = route('user_edit');
        $result['delete_url'] = route('user_delete');
        return $result;
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|max:255',
            'profession' => 'required',
            'city' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 'fail', 'message' => $validator->errors()->first()];
        } else {
            
            $userCreate = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'profession' => $request->profession,
                'city' => $request->city,
                'password' => Hash::make($request->password)
            ]);
            
            if ($userCreate) {
                return ['status' => 'success', 'message' => "User added successfully", 200];
            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,id|max:255',
            'profession' => 'required',
            'city' => 'required',
        ]);

        if ($validator->fails()) {
            return ['status' => 'fail', 'message' => $validator->errors()->first()];
        } else {

            $updated = User::where('id',$request->id)
                        ->update([
                            'name' => $request->name,
                            'email' => $request->email,
                            'profession' => $request->profession,
                            'city' => $request->city
                        ]);
            
            if ($updated) {
                return ['status' => 'success', 'message' => "User has been updated successfully"];
            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        $status = "fail";
        $message = "Something went wrong.";

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 'fail', 'message' => $validator->errors()->first()];
        } else {
            $deleted = User::where('id',$data['id'])->delete();
            
            if ($deleted) {
                return ['status' => 'success', 'message' => "User has been deleted successfully"];
            }
            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }

    public function import(Request $request)
    {
        $data = $request->all();
        $status = "fail";
        $message = "Something went wrong.";
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);

        if ($validator->fails()) {
            return ['status' => 'fail', 'message' => $validator->errors()->first()];
        } else {
            $file = $request->file;

            $fileData = file_get_contents($file->getPathname());
            $fileData = explode("\n", $fileData);
            
            $users = $this->convertCsvToArray($fileData);
            
            if(count($users) > 0){
                $emails = array_column($users,'email');
                
                $ifAlready = DB::table('users')->whereIn('email',$emails)->get();
                if(count($ifAlready) == 0){

                    $response = 0;
                    foreach($users as $val) {
                        $response = User::create($val);
                    }

                    return ['status' => 'success', 'message' => "User has been imported successfully"];

                } else {
                    return ['status' => 'fail', 'message' => "Email-id already registered"];
                }
            }

            return ['status' => 'fail', 'message' => "Data is not available"];
        }
    }

    public function convertCsvToArray($fileData, $delimiter = ',')
    {
        $header = NULL;
        $data = array();

        foreach($fileData as $key => $value){
            $value = explode(',',$value); 

            if (count($value) > 1) {

                if (!$header) {                    
                    $header = $value;
                } else {
                    $data[] = array_combine($header, $value);
                }
            }
        }
        return $data;
    }
}
