<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        return response()->json([
            'name' => $user->name,
            'email' => $user->email,
            'profession' => $user->profession,
            'city' => $user->city
        ], 200);
    }

    public function edit(Request $request)
    {    
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,id|max:255',
            'profession' => 'required',
            'city' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        } else {
            $updated = User::where('id',Auth::user()->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'profession' => $request->profession,
                'city' => $request->city
            ]);
            
            if ($updated) {
                return response()->json(['message' => 'User has been updated successully'], 200);
            } else {
                return response()->json(['message' => 'Some error occured, please try again'], 500);
            } 
        }
    }
}
