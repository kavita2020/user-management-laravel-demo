Project requirements

php (7.4.1)
MySql (5.7.31)
laravel (8.61.0)

Open terminal

- Go to directory in which you want to clone the project.

- Run clone command to download project from git

    git clone https://kavita2020@bitbucket.org/kavita2020/user-management-laravel-demo.git

environment setup

- Make a copy of .env.example file and save it as .env file in project root directory. 
- Update following variable values with configuration credentials in .env file.

For Database

    DB_CONNECTION
    DB_HOST
    DB_PORT
    DB_DATABASE
    DB_USERNAME
    DB_PASSWORD

Install project dependencies

- Enter the project directory

- Run following command to install

    composer install

Database setup

- For creating database

    Create database manually by using command line or phpmyadmin of the same name you mentioned in the DB_DATABASE variable .env file.

- To create tables in database.

    php artisan migrate

Apis

- For api authentication 
    I have used passport laravel package. Firstlt install the passport by following below command:-
    
    php artisan passport:install
    
    With above command you will get client_id and client_secret, which will use for the getting access_token. Now to get the access_token hit the following url with required parameter:-
    
    link - http://localhost:8080/oauth/token
    
    endpoint - /oauth/token
    
    method - POST
    
    parameters

        'grant_type' => 'password',
        'client_id' => 'client-id',
        'client_secret' => 'client-secret',
        'username' => 'user@email.com',
        'password' => 'my-password',

    You will get a access_token in the response. After getting the access_token, you can hit other profile and edit api.


- To Get user data

    link - http://localhost:8080/api/user/profile
    
    endpoint - api/user/profile
    
    method - GET
    
    Headers -
        'Authorization': Bearer access_token
        'Accept': application/json

    Response wil get the user details related to this access_token.


- To Edit user details

    link - http://localhost:8080/api/user/edit
    
    endpoint - /api/user/edit
    
    method - POST
    
    Headers -
        'Authorization': Bearer access_token
        'Accept': application/json
    
    parameters - 
        'name' => 'name',
        'email' => 'test@mail.com',
        'profession' => 'profession',
        'city' => 'city
    
    This will edit the user details related to this access_token's user.