
@extends('layout')

@section('title', 'User List')

@section('content')
    <div class="col">
      <h2>Users List</h2>
      
    </div>
    <div class="col">
      <div class="btn_div">
        <button type="button" class="btn btn-primary btn-small addBtn" data-title="Add User" data-url="{{ route('user_add') }}" data-type="add">Add User</button>
        <button type="button" class="btn btn-primary btn-small importBtn">Import User</button>
        <a href="{{asset('/users.csv')}}" class="btn btn-primary btn-small">Download Sample CSV File</a>
      </div>
    </div>
  
  <input type="hidden" id="list_url" data-path="{{ route('user_list') }}">
  
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Profession</th>
        <th scope="col">City</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody class="list_body">
    </tbody>
  </table>
@endsection

