<html>
    <head>
        <title>Demo Application - @yield('title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{asset('/css/style.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-md-center">
                @yield('content')
                @include('modals.user_modal')
                @include('modals.import_modal')
            </div>
        </div>
    </body>
    <footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="{{asset('/js/main.js')}}"></script>
    </footer>
</html>