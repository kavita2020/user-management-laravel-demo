<div class="modal fade" id="importModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Import Data</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="importForm" method="post" enctype="multipart/form-data" action="{{ route('user_import') }}">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Select File:</label>
            <input type="file" class="form-control" name="file" id="em_file">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary importUser">Save</button>
      </div>
    </div>
  </div>
</div>