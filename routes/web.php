<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiController;

Route::get('/',[UserController::class, 'index'])->name('user_index');
Route::prefix('user')->group(function () {
    Route::get('/list',[UserController::class, 'list'])->name('user_list');
    Route::post('/add',[UserController::class, 'add'])->name('user_add');
    Route::post('/edit',[UserController::class, 'edit'])->name('user_edit');
    Route::post('/delete',[UserController::class, 'delete'])->name('user_delete');
    Route::post('/import',[UserController::class, 'import'])->name('user_import');
});