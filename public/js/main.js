$(document).ready(function() {
    getUserList();
});

function getUserList() {
    var url = $('#list_url').attr('data-path');
    $.ajax({
        url: url,
        type: 'GET',
    }).success(function(res) {
        $('.list_body').html('');
        if (res.data.length > 0) {
            var i = 1;
            $.each(res.data, function(key, value) {
                $('.list_body').append('<tr><th scope="row">'+ i +'</th><td class="name">'+ value.name +'</td><td class="email">'+ value.email +'</td><td class="profession">'+ value.profession +'</td><td class="city">'+ value.city +'</td><td><button type="button" class="btn btn-primary editBtn" data-title="Edit User" data-url="'+ res.edit_url +'" data-id="'+value.id+'" data-type="edit">Edit</button><button type="button" class="btn btn-danger mg-left deleteBtn" data-id="'+value.id+'" data-url="'+ res.delete_url +'">Delete</button></td></tr>');
                i++;
            });
        } else {
            $('.list_body').append('<tr><td colspan="6"><div class="no_record">No user available</div></td></tr>')
        }
    });
}

$(document).on('click', '.addBtn', function() {
    showModal($(this));
});

$(document).on('click', '.editBtn', function() {
    showModal($(this));
});

$(document).on('click', '.importBtn', function() {
    $('#importModal').modal('show');
});

function showModal(button) {
    var thisTr = button.closest('tr');
    var type = button.data('type');
    var name = "";
    var email = "";
    var profession = "";
    var city = "";
    var id = "";

    if (type == "edit") {
        name = thisTr.find('.name').text(); 
        email = thisTr.find('.email').text();
        profession = thisTr.find('.profession').text();
        city = thisTr.find('.city').text();
        id = button.data('id');
    }

    var title = button.data('title');
    var form_url = button.data('url');
    var modal = $('#userModal');

    modal.find('.modal-body #em_name').val(name);
    modal.find('.modal-body #em_email').val(email);
    modal.find('.modal-body #em_profession').val(profession);
    modal.find('.modal-body #em_city').val(city);
    modal.find('.modal-body #em_id').val(id);
    modal.find('.modal-title').text(title)
    modal.find('#userForm').attr('action',form_url);
    modal.find('.modal-body .alert').hide();
    modal.modal('show');
}

$(document).on('click', '.saveUser', function() {
    let myForm = $('#userForm');
    var editUrl = myForm.attr('action');
    var fd = new FormData(myForm[0]);
    var modal = $('#userModal');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: editUrl,
        type: 'POST',
        data: fd,
        processData: false,
        contentType: false,
        success: function(res) {
            if (res.status == "success") {
                modal.modal('hide');
                getUserList();
            } else {
                showAlertMessage(modal, res.message);
            }
        }
    });

});

function showAlertMessage(element, message) {
    element.find('.modal-body').prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert"><span class="error_message">'+message+'</span><button type="button" class="close alert_close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
}

$(document).on('click', '.deleteBtn', function(){
    var thisUrl = $(this).data('url');
    var id = $(this).data('id');

    if (confirm("Do you want to delete this user ?")) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: thisUrl,
            data: {'id':id},
            type: 'POST',
            success: function(res) {
                if(res.status == "success"){
                    getUserList();
                }
            }
        });
    }
});

$(document).on('click', '.importUser', function() {
    let myForm = $('#importForm');
    var importUrl = myForm.attr('action');
    var fd = new FormData(myForm[0]);
    var modal = $('#importModal');
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: importUrl,
        type: 'POST',
        data: fd,
        processData: false,
        contentType: false,
        success: function(res) {
            if(res.status == "success"){
                modal.modal('hide');
                getUserList();
            }else{
                showAlertMessage(modal, res.message);
            }
        }
    });

});